using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NPCRespound;

public class DogRespoundToSound : MonoBehaviour, IHear
{
    private AIAgent agent;
    [HideInInspector] public Vector2 _sounds;
    

    private void Start()
    {
        agent = GameObject.Find("DogController").GetComponent<AIAgent>();
    }

    public void RespondToSound(Sound sound)
    {
        if (sound.soundType == Sound.SoundType.Middle) // check the sound "Type"
        {
            agent.stateMachine.ChangeState(AiStateId.RespondToSoundState); // change state
        }

        _sounds = sound.pos; // set actual position of the sound
    }
}

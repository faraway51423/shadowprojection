using UnityEngine;

[CreateAssetMenu()]
public class DogConfig : ScriptableObject
{
    public float speed = 200f;
    public float patrolSpeed = 10f;
    public float nextWaypointDisctance = -0.01f;
    public float waitTime = 3f;
    public float airTimeSpeed = 350f;
}

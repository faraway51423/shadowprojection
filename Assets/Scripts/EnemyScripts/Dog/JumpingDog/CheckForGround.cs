using System.Collections;   
using System.Collections.Generic;
using UnityEngine;

public class CheckForGround : MonoBehaviour
{
    [SerializeField] private AIAgent agent;


    //private void Update()
    //{
    //    Debug.DrawRay(transform.position, Vector2.down * 1.5f, Color.red);

    //    if (Physics2D.Raycast(transform.position, Vector2.down, 1.5f, agent.platformLayer))
    //    {
    //        agent.stateMachine.ChangeState(AiStateId.Patrol);
    //        print("Patrol");
    //    }
    //    else
    //    {
    //        print("Jump");
    //        agent.stateMachine.ChangeState(AiStateId.JumpState);
    //    }
    //}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if ((agent.platformLayer) != 0)
        {
            agent.stateMachine.ChangeState(AiStateId.Patrol);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((agent.platformLayer) != 0)
        {
            agent.stateMachine.ChangeState(AiStateId.JumpState);
        }
    }
}

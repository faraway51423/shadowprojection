using System.Collections;
using TMPro;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

public class JumpState : AiState
{
    public void Enter(AIAgent agent)
    {
        agent.walk = true;
        agent.StartCoroutine(WaitForJump(agent));
    }

    public void Exit(AIAgent agent)
    {
        agent.StopCoroutine(WaitForJump(agent));
        hasJumped = false;
    }

    public AiStateId GetId()
    {
        return AiStateId.JumpState;
    }

    public void Update(AIAgent agent)
    {
        Vector2 currentDirection = agent.patrolPoints[agent.currentPointIndex].position; //current point  mob need to move on
        Vector2 direction = (currentDirection - agent.agentRb.position.normalized); //current direction where mob look (x)

        if (hasJumped)
        {
            agent.agentTransform.position = Vector2.MoveTowards(agent.agentTransform.position, direction, agent.config.airTimeSpeed * Time.deltaTime);//move mob/direction/
        }

        //if (direction.x >= 0.0f) // �� ���������� ������ ��������, ������ ���������� ����
        //{
        //    agent.agentTransform.localScale = new Vector3(-7f, 7f, 1f);
        //}
        //else
        //{
        //    agent.agentTransform.localScale = new Vector3(7f, 7f, 1f);
        //}
    }

    private bool hasJumped;
    private float nearestDistance;
    private Collider2D nearestPlatform;

    private IEnumerator WaitForJump(AIAgent agent)
    {
        yield return new WaitForSeconds(1f);

        // ����� ��������� ��������� � ������� ���� ����
        Collider2D platform = FindNearestPlatformInDirection(agent);

        if (platform != null && !hasJumped)
        {
            Debug.Log("jump");
            Jump(agent, platform);
        }
    }

    private void Jump(AIAgent agent, Collider2D platform)
    {
        // ������������ ���� ������ �� ������ ���������� �� ��������� ���������
        float _jumpForce = CalculateJumpForce(Vector2.Distance(agent.transform.position, platform.transform.position));

        // ��������� ���� ������ � ������������ � ��������� ���������
        Vector2 jumpDirection = (platform.transform.position - agent.transform.position).normalized;
        agent.agentRb.AddForce(jumpDirection * _jumpForce, ForceMode2D.Impulse); //Add force to the player when he jumps 

        // ������������� ����, ����� ������������� ��������� ���������� ������
        hasJumped = true;
    }
    float CalculateJumpForce(float distanceToPlatform)
    {
        // ��� ��������� ����� ���� ��������� � ����������� �� ����� ������������.
        float desiredJumpHeight = 15.0f; // �������� ������ ������.
        float gravity = 3f; // ����������.

        // ����������� ������ ������ � �������������� ������������ �������.
        float jumpHeight = Mathf.Clamp(distanceToPlatform, 0f, desiredJumpHeight); // ������������ ������, ����� �� �������� ������� ������.

        // ������������ �����������, ������� ����� ���� �������� � ����������� �� ����� ������������.
        float dynamicCoefficient = 0.8f; // ���� ����������� ���������� ����������� ������ ������ �� ����������.

        float jumpForce = Mathf.Sqrt(2 * gravity * jumpHeight) + dynamicCoefficient * distanceToPlatform;

        return jumpForce;
    }

    private Collider2D FindNearestPlatformInDirection(AIAgent agent)
    {
        Collider2D nearestPlatform = null;
        float nearestDistance = Mathf.Infinity;

        // ������� ��������� � ������� ������
        Collider2D[] colliders = Physics2D.OverlapCircleAll(agent.transform.position, 15f, agent.platformLayer);

        Debug.Log(colliders.Length);

        // ������� ��������� � ������� ���� ����
        foreach (Collider2D collider in colliders)
        {
            float distance = Vector2.Distance(agent.transform.position, collider.transform.position);
            if (distance < nearestDistance)
            {
                Vector2 directionToPlatform = (collider.transform.position - agent.patrolPoints[agent.currentPointIndex].position).normalized;
                Vector2 direction = ((Vector2)agent.patrolPoints[agent.currentPointIndex].position - (Vector2)agent.agentRb.position).normalized;

                if (Vector2.Dot(directionToPlatform, direction) > 0)
                {
                    nearestDistance = distance;
                    nearestPlatform = collider;
                }
            }
        }

        return nearestPlatform;
    }
}



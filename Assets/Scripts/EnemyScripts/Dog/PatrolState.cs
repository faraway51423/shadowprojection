using UnityEngine;
using static UnityEngine.RuleTile.TilingRuleOutput;

public class PatrolState : AiState
{
    private float requiredPastDistance = 3f;
    
    public void Enter(AIAgent agent)
    {
        agent.InvokeRepeating("UpdatePathToPoint", 0f, 0.3f); //start repeating "UpdatePath" method
    }

    public void Exit(AIAgent agent)
    {
        agent.CancelInvoke("UpdatePathToPoint"); //Stop repeat "UpdatePathToPoint" method
    }

    public AiStateId GetId()
    {
        return AiStateId.Patrol;
    }

    public void Update(AIAgent agent)
    {
        if (agent.path == null)
        {
            return;
        }

        if (agent.currentWaypoint >= agent.path.vectorPath.Count)
        {
            agent.reachedEndOfPath = true;
            return;
        }
        else
        {
            agent.reachedEndOfPath = false;
        }
        Vector2 currentDirection = agent.patrolPoints[agent.currentPointIndex].position; //current point  mob need to move on
        //Vector2 facedDirection = (currentDirection - (Vector2)agent.agentRb.position).normalized;
        Vector2 direction = (currentDirection - agent.agentRb.position.normalized); //current direction where mob look (x)
        Vector2 force = direction * agent.config.speed * Time.deltaTime;
        float distance = Vector2.Distance(agent.agentRb.position, agent.path.vectorPath[agent.currentWaypoint]);
        float pastDistance = Vector2.Distance(agent.agentRb.position, agent.patrolPoints[agent.currentPointIndex].position);


        if (distance < agent.config.nextWaypointDisctance)
        {
            agent.currentWaypoint++;
        }

        //if (facedDirection.x >= 0.0f) // �� ���������� ������ ��������, ������ ���������� ����
        //{
        //    agent.agentTransform.localScale = new Vector3(-7f, 7f, 1f); 
        //}
        //else
        //{
        //    agent.agentTransform.localScale = new Vector3(7f, 7f, 1f);
        //}

        if (pastDistance - 0.5f >= requiredPastDistance)
        {
            agent.agentTransform.position = Vector2.MoveTowards(agent.agentTransform.position, direction, agent.config.patrolSpeed * Time.deltaTime);//move mob/direction/speed
            agent.walk = false; // set animation
        }
        else
        {
            agent.walk = true;//set animation
            if (agent.once == false)
            {
                agent.once = true;
                agent.StartCoroutine(agent.WaitForNextWaypoint(agent));
            }
        }
    }
}

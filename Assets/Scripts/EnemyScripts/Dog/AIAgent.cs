using Pathfinding;
using System.Collections;
using UnityEngine;
using UnityEngine.Tilemaps;

public class AIAgent : MonoBehaviour
{
    public LayerMask platformLayer; // ���� ���������

    public AiStateId initialState;
    public AiStateMachine stateMachine;
    public DogConfig config;

    public Path path;
    public Transform[] patrolPoints;

    [SerializeField] private GameObject agentController;

    [HideInInspector] public Animator animator;
    [HideInInspector] public DogRespoundToSound sounds;
    [HideInInspector] public Rigidbody2D agentRb;
    [HideInInspector] public Transform agentTransform;
    [HideInInspector] public Seeker agentSeeker; 
    [HideInInspector] public Transform playerTransform;

    [HideInInspector] public bool reachedEndOfPath;
    [HideInInspector] public int currentWaypoint = 0;
    [HideInInspector] public int currentPointIndex = 1;
    [HideInInspector] public bool once;
    [HideInInspector] public bool walk;

    void Start()
    {
        playerTransform = GameObject.Find("Player").GetComponent<Transform>();
        sounds = agentController.GetComponent<DogRespoundToSound>();
        agentRb = agentController.GetComponent<Rigidbody2D>();
        agentTransform = agentController.GetComponent<Transform>();
        agentSeeker = agentController.GetComponent<Seeker>();
        animator = agentController.GetComponent<Animator>();

        stateMachine = new AiStateMachine(this);
        stateMachine.RegisterState(new ChasePlayerState());
        stateMachine.RegisterState(new RespondToSoundState());
        stateMachine.RegisterState(new PatrolState());
        stateMachine.RegisterState(new JumpState());
        stateMachine.ChangeState(initialState);
    }

    void Update()
    {
        stateMachine.Update();

        if (walk == false)
        {
            animator.SetBool("IsStay", false);
        }
        else
        {
            animator.SetBool("IsStay", true);
        }
        agentRb.WakeUp();

        Vector2 currentDirection = patrolPoints[currentPointIndex].position; //current point  mob need to move on
        Vector2 facedDirection = (currentDirection - (Vector2)agentRb.position).normalized;

        if (facedDirection.x >= 0.0f) // �� ���������� ������ ��������, ������ ���������� ����
        {
            agentTransform.localScale = new Vector3(-7f, 7f, 1f);
        }
        else
        {
            agentTransform.localScale = new Vector3(7f, 7f, 1f);
        }
    }

    public void UpdatePath()
    {
        if (agentSeeker.IsDone())
        {
            agentSeeker.StartPath(agentRb.position, playerTransform.position, OnPathComplete);
        }
    }

    public void UpdatePathToPoint()
    {
        if (agentSeeker.IsDone())
        {
            agentSeeker.StartPath(agentRb.position, patrolPoints[currentPointIndex].position, OnPathComplete);
        }
    }

    public void UpdatePathToSoundPos()
    {
        if (agentSeeker.IsDone())
        {
            agentSeeker.StartPath(agentRb.position, sounds._sounds, OnPathComplete);
        }
    }

    public void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }
    public IEnumerator WaitForNextWaypoint(AIAgent agent)
    {
        yield return new WaitForSeconds(agent.config.waitTime);
        if (agent.currentPointIndex + 1 < agent.patrolPoints.Length)
        {
            agent.currentPointIndex++;
        }
        else
        {
            agent.currentPointIndex = 0;
        }
        once = false;
    }

    public IEnumerator SearchForSound(AIAgent agent)
    {
        yield return new WaitForSeconds(agent.config.waitTime);
        once = false;
        agent.stateMachine.ChangeState(AiStateId.Patrol);
    }

}

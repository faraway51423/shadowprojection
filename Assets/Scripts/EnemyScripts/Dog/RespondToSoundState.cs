using UnityEngine;

public class RespondToSoundState : AiState
{
    private float requiredPastDistance = 3f;
    public void Enter(AIAgent agent)
    {
       agent.InvokeRepeating("UpdatePathToSoundPos", 0f, 0.3f); //Start repeating "UpdatePathToSoundPos" method
    }
    public void Exit(AIAgent agent)
    {
        agent.CancelInvoke("UpdatePathToSoundPos");//stop repating method when leaves the state
    }

    public AiStateId GetId()
    {
        return AiStateId.RespondToSoundState;
    }

    public void Update(AIAgent agent)
    {
        if (agent.path == null)
        {
            return;
        }

        if (agent.currentWaypoint >= agent.path.vectorPath.Count)
        {
            agent.reachedEndOfPath = true;
            return;
        }
        else
        {
            agent.reachedEndOfPath = false;
        }

        Vector2 currentDirection = agent.sounds._sounds; 
        Vector2 direction = (currentDirection - agent.agentRb.position);
        Vector2 force = direction * agent.config.speed * Time.deltaTime;
        float pastDistance = Vector2.Distance(agent.agentRb.position, agent.sounds._sounds);


        if (force.x >= 0.01f)
        {
            agent.agentTransform.localScale = new Vector3(-7, 7f, 1f);//flip sprite of the mob depents on what direction it moves
        }
        else if (force.x <= -0.01f)
        {
            agent.agentTransform.localScale = new Vector3(7f, 7f, 1f);
        }

        if (pastDistance - 0.5f >= requiredPastDistance)
        {
            agent.agentTransform.position = Vector2.MoveTowards(agent.agentTransform.position, currentDirection, agent.config.patrolSpeed * Time.deltaTime); // move|player|to last sound position|speed
            agent.walk = false;//set animation
        }
        else
        {
            agent.walk = true;//set animation
            if (agent.once == false)
            {
                agent.once = true;
                agent.StartCoroutine(agent.SearchForSound(agent)); 
            }
        }
    }
}

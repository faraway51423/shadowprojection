using UnityEngine;
public class ChasePlayerState : AiState
{
    public void Enter(AIAgent agent)
    {
        agent.InvokeRepeating("UpdatePath", 0f, 0.3f); //start repeating "UpdatePath" method
        agent.walk = false; //set animation
        Debug.Log("enter chase");
    }

    public void Exit(AIAgent agent)
    {
        agent.CancelInvoke("UpdatePath");//cancel repeating method
    }
    
    public AiStateId GetId()
    {
        return AiStateId.ChasePlayer;
    }

    public void Update(AIAgent agent)
    {
        Debug.Log("Chasing");
        if (agent.path == null)
        {
            return;
        }

        if (agent.currentWaypoint >= agent.path.vectorPath.Count)
        {
            agent.reachedEndOfPath = true;
            return;
        }
        else
        {
            agent.reachedEndOfPath = false;
        }

        Vector2 direction = ((Vector2)agent.path.vectorPath[agent.currentWaypoint] - agent.agentRb.position).normalized;
        Vector2 force = direction * agent.config.speed * Time.deltaTime;
        float distance = Vector2.Distance(agent.agentRb.position, agent.path.vectorPath[agent.currentWaypoint]);
        agent.agentRb.AddForce(force);

        if (distance < agent.config.nextWaypointDisctance)
        {
            agent.currentWaypoint++;
        }

        //if (force.x >= 0.00f)
        //{
        //    agent.agentTransform.localScale = new Vector3(-7, 7f, 1f);
        //}
        //else if (force.x <= -0.00f)
        //{
        //    agent.agentTransform.localScale = new Vector3(7f, 7f, 1f);
        //}
    }
}

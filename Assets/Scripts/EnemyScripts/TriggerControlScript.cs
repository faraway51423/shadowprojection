using UnityEngine;

public class TriggerControlScript : MonoBehaviour
{
    private AIAgent agent;

    private void Start()
    {
       agent = GameObject.Find("DogController").GetComponent<AIAgent>();
    }

    private void OnTriggerEnter2D(Collider2D collision) //change between states if players enter or exit trigger
    {
        if (collision.gameObject.tag == "PlayerGroundCheck")
        {
            agent.stateMachine.ChangeState(AiStateId.ChasePlayer);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerGroundCheck")
        {
            agent.stateMachine.ChangeState(AiStateId.Patrol);
        }
    }
}

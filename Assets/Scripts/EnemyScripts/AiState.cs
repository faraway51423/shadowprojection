using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AiStateId //states
{
    ChasePlayer,
    Patrol,
    RespondToSoundState,
    JumpState
}

public interface AiState //deffault interface for state
{
    AiStateId GetId();
    void Enter(AIAgent agent);
    void Update(AIAgent agent);
    void Exit(AIAgent agent);
}

using UnityEngine;

namespace NPCRespound
{
    public static class Sounds
    {
        public static void MakeSound(Sound sound)
        {
            int layer1 = 1 << LayerMask.NameToLayer("Enemy");
            int layer2 = 1 << LayerMask.NameToLayer("Default");

            // ���������� ����
            LayerMask layerMask = layer1 | layer2;

            Collider2D[] col = Physics2D.OverlapCircleAll(sound.pos, sound.range, layerMask);

            for (int i = 0; i < col.Length; i++)
            {
                Debug.Log("we are here");
                if (col[i].TryGetComponent(out IHear hearer))
                {
                    Debug.Log("hear ya");
                    hearer.RespondToSound(sound);
                }
            }
        }
        
    }
}

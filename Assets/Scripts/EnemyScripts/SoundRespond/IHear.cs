namespace NPCRespound
{
    public interface IHear
    {
        void RespondToSound(Sound sound);
    }
}


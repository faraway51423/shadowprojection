using UnityEngine;

namespace NPCRespound
{
    public class SoundMaker : MonoBehaviour
    {
        [SerializeField] private AudioSource source = null;
        [SerializeField] private float soundRange = 15f;

        private void OnMouseDown()
        {
            if (source.isPlaying)
                return;


            source.Play();

            var sound = new Sound(transform.position, soundRange, Sound.SoundType.Middle); 

            Sounds.MakeSound(sound);
        }
    }
}

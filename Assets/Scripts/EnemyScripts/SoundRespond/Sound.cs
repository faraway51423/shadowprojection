using UnityEngine;


namespace NPCRespound
{
    public class Sound
    {
        public readonly Vector2 pos;
        public readonly float range;
        public enum SoundType {Default = -1, Near, Middle, Far}
        public SoundType soundType;

        public Sound(Vector2 _pos, float _range, SoundType _soundType)
        {
            pos = _pos;
            range = _range;
            soundType = _soundType;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomTransition : MonoBehaviour
{
    public CameraController cameraController;

    [SerializeField] private Transform player;
    [SerializeField] private float proximityThreshold = 10f;
    [SerializeField] private float fieldOfViewMin = 102.7f;
    [SerializeField] private float fieldOfViewMax = 130f;
    [SerializeField] private float zoomSpeed = 10f;
    private float targetFieldOfView = 50f;

    private void Update()
    {
    }
}

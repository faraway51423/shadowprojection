using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    private float targetFieldOfView = 50f;
    private float fieldOfViewMin = 102.7f;

    [SerializeField] private CinemachineVirtualCamera cam;
    [SerializeField] private Transform player;
    [SerializeField] private float proximityThreshold = 10f;
    [SerializeField] private float fieldOfViewMax = 130f;
    [SerializeField] private float zoomSpeed = 10f;

    private void Update()
    {
        Zoom();
    }

    public void Zoom()
    {
        float distance = Vector3.Distance(player.position, gameObject.transform.position);

        if (distance < proximityThreshold)
        {
            if (cam.m_Lens.FieldOfView < fieldOfViewMax)
            {
                targetFieldOfView = Mathf.Min(fieldOfViewMax, targetFieldOfView + Time.deltaTime * zoomSpeed);
            }
        }
        else
        {
            if (cam.m_Lens.FieldOfView > fieldOfViewMin)
            {
                targetFieldOfView = Mathf.Max(fieldOfViewMin, targetFieldOfView - Time.deltaTime * zoomSpeed);
            }
        }
        targetFieldOfView = Mathf.Clamp(targetFieldOfView, fieldOfViewMin, fieldOfViewMax);
        cam.m_Lens.FieldOfView = Mathf.Lerp(cam.m_Lens.FieldOfView, targetFieldOfView, Time.deltaTime * zoomSpeed);
    }
}

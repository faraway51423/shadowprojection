using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField] private CharacterController2D controller;
    [SerializeField] private UIController uiController;
    private DialogueManager dialogueManager;
    private GrapplingGun grapplingGun;
    [HideInInspector] public NPCTriggerDialogue interactableNPC; // nearest NPC to the character

    [Header("Objects")]
    [SerializeField] private GameObject firstPlan;
    [SerializeField] private GameObject secondPlan;

    [Header("Other")]
    [SerializeField] private float runSpeed = 40f;
    [HideInInspector] public Animator animator;

    [Header("Teleport/TimeFreeze")]
    [SerializeField] private float timeFreezeScale;
    [SerializeField] private float freezeTimeAmount;

    [SerializeField] private float freezeTimeCooldown;
    private float freezeTime = 3f;
    private bool freezeTimeReady = true;
    private bool canTeleportTime;
    private bool hasTeleported;
    private bool canSwitch = true;
    private float originalTimeScale;
    public float teleportCooldown;
    private float teleportTime;


    [HideInInspector] public float horizontalMove = 0f;
    [HideInInspector] public bool inDialogue;
    private Rigidbody2D rb;

    //Movement
    private bool jump = false;
    private bool cutJump = false;
    private bool crouch = false;

    private Dictionary<KeyCode, bool> keys = new Dictionary<KeyCode, bool>(); // input Dictionary

    private void Awake()
    {
        dialogueManager = FindObjectOfType<DialogueManager>();
        grapplingGun = FindObjectOfType<GrapplingGun>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        originalTimeScale = Time.timeScale;
    }

    private void Update()
    {
        if (!inDialogue) //Check if player talk to someone
        {
            horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed; //Input from player to run
            animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

            if (Input.GetButtonDown("Jump"))  //Inputs from player to jump
            {
                jump = true;
                animator.SetBool("IsJumping", true);
            }
            if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f) //Input from player to cut jump
            {
                cutJump = true;
            }

            if (Input.GetButtonDown("Crouch")) //Input from player for crouch
            {
                crouch = true;
            }
            else if (Input.GetButtonUp("Crouch"))
            {
                crouch = false;
            }
            if (GetKeyDown(KeyCode.B)) // Input to switch between dimensions
            {
                SwitchDimensions();
                if (canTeleportTime)
                {
                    hasTeleported = true;
                }
            }
            if (GetKeyDown(KeyCode.Mouse1))
            {
                StartCoroutine(FreezeTime());
            }
            if (GetKeyDown(KeyCode.E)) //Input to interact
            {
                interactableNPC.TriggerDialogue(); //Start dialoge if player in dialoge trigger zone
                StartCoroutine(dialogueManager.OpportunityToSkipDialogue());//Start coroutine for fast dialogue skip;
            }
            //grapplingGun.UseGrapplingGun();
        }
        else
        {
            if (dialogueManager.canSkip == true)//Can skip first 1.4 seconds after starting conversation
            {
                if (Input.GetKeyDown(KeyCode.Space))//Increase skip attempts, when they become equeal to 5 stop dialogue.
                {
                    dialogueManager.skipTryCount++;
                }
                if (dialogueManager.skipTryCount >= 5)
                {
                    dialogueManager.StopDialogue();
                }
            }
        }
        if (!freezeTimeReady)
        {
            freezeTime -= Time.deltaTime;
            if (freezeTime <= 0 )
            {
                freezeTimeReady = true;
                freezeTime = freezeTimeCooldown;
                StartCoroutine(uiController.IncreaseBar(uiController.freezeTimeBar, freezeTimeCooldown));
            }
        }
    }

    private void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump, cutJump); //Move method
        jump = false;
        cutJump = false;
    }

    public void OnLanding()//Method for landing event animation
    {
        animator.SetBool("IsJumping", false);
    }

    public void OnCrouching(bool isCrouching)//Method for crouching event animation
    {
        animator.SetBool("IsCrouching", isCrouching);
    }

    #region Switch Between Dimensions(On/Off)

    private void OnTriggerStay2D(Collider2D collision) //Check triggers when player want to switch dimension
    {
        if (collision.gameObject.tag == "Trigger")
        {
            canSwitch = false;
            //DisableKey(KeyCode.B);//Disable ablitiy to switch between dimensions
        }
    }

    private void OnTriggerExit2D(Collider2D collision) //Check if player exit 
    {
        if (collision.gameObject.tag == "Trigger")
        {
            //EnableKey(KeyCode.B);//Enable abaility to switch between dimensions
            canSwitch = true;
        }
    }


    #endregion

    #region Input Dictionary

    private bool GetKeyDown(KeyCode keyCode) //Method for add key to dictionary
    {
        if (!keys.ContainsKey(keyCode)) //check if already have that key in dictioanry
            keys.Add(keyCode, true); //if not, add new key to dictionary
        return Input.GetKeyDown(keyCode) && keys[keyCode];
    }

    public void DisableKey(KeyCode keyCode) //Method for disabling key 
    {
        keys[keyCode] = false;
    }

    public void EnableKey(KeyCode keyCode) //Method for enabling key
    {
        keys[keyCode] = true;
    }

    #endregion

    private void SwitchDimensions()
    {
        if (canSwitch)
        {
            StartCoroutine(uiController.DecreaseTeleportBar());

            if (firstPlan.activeInHierarchy)
            {
                firstPlan.SetActive(false);
                secondPlan.SetActive(true);
            }
            else
            {
                firstPlan.SetActive(true);
                secondPlan.SetActive(false);
            }
        }
    }
    private IEnumerator FreezeTime()
    {
        StartCoroutine(uiController.DecreaseBar(uiController.freezeTimeBar, freezeTimeAmount));
        DisableKey(KeyCode.Mouse1);
        Time.timeScale = timeFreezeScale;
        canTeleportTime = true;
        yield return new WaitForSeconds(freezeTimeAmount);
        Time.timeScale = originalTimeScale; 
        if (!hasTeleported)
        {
            SwitchDimensions();
        }
        canTeleportTime = false;
        hasTeleported = false;
        freezeTimeReady = false;
    }
}

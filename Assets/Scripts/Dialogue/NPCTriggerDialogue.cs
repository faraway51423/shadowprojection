using UnityEngine;

public class NPCTriggerDialogue : MonoBehaviour
{
    public Message[] messages;
    public Actor[] actors;
    private PlayerMovement player;

    private void Awake()
    {
        player = FindObjectOfType<PlayerMovement>();
    }

    private void OnTriggerStay2D(Collider2D collision) //Sets this particular NPC to "nearest NPC to the player" when player passes near by him and enadle "interaction key".
    {
        if (collision.name == "Player")
        {
            player.interactableNPC = this;
            player.EnableKey(KeyCode.E);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) //Disable "interaction key" when player exit trigger zone
    {
        if (collision.name == "Player")
        {
            player.DisableKey(KeyCode.E);
        }
    }

    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(messages, actors);
    }
}

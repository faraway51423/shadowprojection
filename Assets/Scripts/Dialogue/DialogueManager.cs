using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    [Header("UI Elements")]
    [SerializeField] private Text nameText;
    [SerializeField] private Text dialogueText;
    [SerializeField] public GameObject skipPanel;

    [Header("Objects")]
    [SerializeField] private PlayerMovement player;
    [SerializeField] private GameObject dialogueWindow;

    [Header("Other Variables")]
    [SerializeField] private Animator animator;
    [SerializeField] private float textSpeed;

    private Message[] currentMessages;
    private Actor[] currentActor;
    private int activeMessage;
    private static bool isActive = false;
    private bool canSkipLine = true;
    [HideInInspector] public bool canSkip;
    [HideInInspector] public int skipTryCount;
    

    private void Update()
    {
        if (canSkipLine)//Check if player can skip lines.
        {
            if (Input.GetMouseButtonDown(0) && isActive || Input.GetKeyDown(KeyCode.Space) && isActive) //If does, check inputs from player
            {
                if (dialogueText.text == currentMessages[activeMessage].message) // If message fully displayed, show next message.
                {
                    DisplayNextSentence();
                }
                else // If not, skip animation and display full message.
                {
                    StopAllCoroutines();
                    dialogueText.text = currentMessages[activeMessage].message;
                }
            }
        }
    }

    public void StartDialogue(Message[] messages, Actor[] actors) //Start new dialogue function
    {
        //set all boolean variables and disable skip panel
        skipPanel.SetActive(false); 
        player.inDialogue = true;
        canSkipLine = true;
        isActive = true;
        //disabled run animation and stop player
        player.horizontalMove = 0f;
        player.animator.SetFloat("Speed", player.horizontalMove);
        //Take variables for dialogue from current NPC that player talk with
        currentMessages = messages;
        currentActor = actors;
        activeMessage = 0;
        //Animate dialogue window to the screen
        dialogueWindow.SetActive(true); 
        animator.SetBool("isOpening", true);
        animator.SetBool("isOpen", true);
        //Display first sentence of dialogue
        DisplayNextSentence();
    }

    private void DisplayNextSentence()//Display next sentence of dialogue, if have one
    {
        activeMessage++;
        if (activeMessage < currentMessages.Length)
        {
            StartCoroutine(TypeSentence());
            Message messageToDisplay = currentMessages[activeMessage];
            Actor actorToDisplay = currentActor[messageToDisplay.actorId];
            nameText.text = actorToDisplay.name;
            dialogueText.color = actorToDisplay.textColor;
        }
        else
        {
            isActive = false;
            EndDialogue();
        }
    }

    public void EndDialogue() //Function for UI button and more
    {
        animator.SetBool("isOpening", false);
        animator.SetBool("isOpen", false);
        StartCoroutine(WaitForDisable());
    }

    public void ContinueDialogue() //Function for UI button
    {
        skipPanel.SetActive(false);
        canSkipLine = true;
    }

    public void StopDialogue()//Stop dialogue by display skip panel
    {
        skipPanel.SetActive(true);
        canSkipLine = false; //disables ability to skip sentences, while skip panel is active
        skipTryCount = 0;
    }

    #region Coroutines

    private IEnumerator TypeSentence() //Coroutine for displaying sentences with animation
    {
        dialogueText.text = string.Empty;
        foreach (char letter in currentMessages[activeMessage].message.ToCharArray())
        {
            dialogueText.text += letter;
            yield return new WaitForSeconds(textSpeed);
        }
    }

    private IEnumerator WaitForDisable() // Coroutine for disable dialogue window when its innactive
    {
        yield return new WaitForSeconds(1f);
        dialogueWindow.SetActive(false);
        player.inDialogue = false;
    }

    public IEnumerator OpportunityToSkipDialogue() //Oportunity for 1.4 seconds to fast skip dialogue
    {
        canSkip = true;
        yield return new WaitForSeconds(1.4f);
        canSkip = false;
    }

    #endregion
}

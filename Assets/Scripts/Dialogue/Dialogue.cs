using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Message
{
    public int actorId;
    [TextArea(2,5)]
    public string message;
}

[System.Serializable]
public class Actor
{
    public string name;
    public Color textColor;
}

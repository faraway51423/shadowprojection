using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameController : MonoBehaviour
{
    [SerializeField] private TilemapCollider2D aStarCollider;


    void Start()
    {
        aStarCollider.isTrigger = true; //Switch phantom colliders to triggers
    }
}

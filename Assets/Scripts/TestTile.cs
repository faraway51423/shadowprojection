using UnityEngine;
using UnityEngine.Tilemaps;

public class TestTile : MonoBehaviour
{
    public Tilemap tilemap;
    public TileBase platformTile;
    public BoundsInt area;

    private void Update()
    {
        Vector3 currentPos = transform.position;

        area.position = Vector3Int.RoundToInt(currentPos);

        ExampleUsage();
    }

    public bool SearchTilesInArea()
    {
        TileBase[] tiles = tilemap.GetTilesBlock(area);//new BoundsInt(bottomLeft, topRight - bottomLeft

        foreach (TileBase tile in tiles)
        {
            if (tile != null)
            {
                if (tile == platformTile)
                {
                    return true;
                }
                // Debug.Log("������ ����: " + tile.name);
            }
        }
        return false;
    }

    void ExampleUsage()
    {
        // ���������, �������� �� ������ ����������
        if (SearchTilesInArea())
        {
            // ������ �������� ����������
            Debug.Log("��� ���� ���������!");
        }
        else
        {
            // ������ �� �������� ����������
            Debug.Log("��� ���� ���������!");
        }
    }
}

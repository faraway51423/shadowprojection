using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private PlayerMovement player;

    public Image teleportBar;
    public Image freezeTimeBar;


    public IEnumerator DecreaseBar(Image img, float fillTime)
    {
        float timer = 0;

        while (timer < fillTime)
        {
            timer += Time.deltaTime; 

            float progress = timer / fillTime;

            float reversedProgress = 1f - progress;

            img.fillAmount = reversedProgress;

            yield return null;
        }
    }

    public IEnumerator DecreaseTeleportBar()
    {
        float timer = 0;

        while (timer < 0.3f)
        {
            timer += Time.deltaTime;

            float progress = timer / 0.3f;

            float reversedProgress = 1f - progress;

            teleportBar.fillAmount = reversedProgress;

            if (teleportBar.fillAmount == 0f)
            {
                StartCoroutine(IncreaseTeleportBar(teleportBar, player.teleportCooldown));
                player.DisableKey(KeyCode.B);
            }

            yield return null;
        }
    }

    public IEnumerator IncreaseTeleportBar(Image img, float fillTime)
    {
        float timer = 0;

        while (timer < fillTime)
        {
            timer += Time.unscaledDeltaTime;

            float progress = timer / fillTime;

            float reversedProgress = progress;

            img.fillAmount = progress;

            if (img.fillAmount == 1f)
            {
                player.EnableKey(KeyCode.B);
            }

            yield return null;
        }
    }

    public IEnumerator IncreaseBar(Image img, float fillTime)
    {
        float timer = 0;

        while (timer < fillTime)
        {
            timer += Time.unscaledDeltaTime; 

            float progress = timer / fillTime;

            float reversedProgress = progress;

            img.fillAmount = progress;

            if (img.fillAmount == 1f)
            {
                player.EnableKey(KeyCode.Mouse1);
            }
            yield return null;
        }
    }
}
